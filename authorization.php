<?php
include("MySQLHandler.php");
session_start();
Log::login("authorization.log", "POST: " . json_encode($_POST));
$mySqlHandler = new MySQLHandler();
$user = $mySqlHandler->authorization($_POST["login"], $_POST["password"]);
if( $user !== false){
    $token = uniqid('', true);
    Log::login("authorization.log", "token: " . $token);
    Log::login("authorization.log", "user: " . json_encode($user));
    $_SESSION["token"] = $token;
    $_SESSION["user_id"] = $user["id"];
    Log::login("authorization.log", "SESSION: " . json_encode($_SESSION));
    die(json_encode(array("isSuccess" => true, "token" => $token)));
} else {
    die(json_encode(array("isSuccess" => false)));
}

?>