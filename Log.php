<?php
class Log{
    const isLoginGlobal = true;
    public static function login($file, $logText, $isLogin = false){
        if(isLoginGlobal || $isLogin){
            if (!file_exists($file)){
                fopen($file, "w");
            }
            file_put_contents($file, date('d.m.Y H:i:s') . ": " . $logText . "\n", FILE_APPEND);
        }
    }
}
?>