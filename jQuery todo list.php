
<!DOCTYPE html>
<html>
<head>
<style>
body {
  margin: 0;
  min-width: 250px;
}

/* Include the padding and border in an element's total width and height */
* {
  box-sizing: border-box;
}

/* Remove margins and padding from the list */
ul {
  margin: 0;
  padding: 0;
}

/* Style the list items */
ul li {
  cursor: pointer;
  position: relative;
  padding: 12px 8px 12px 40px;
  list-style-type: none;
  background: #eee;
  font-size: 18px;
  transition: 0.2s;
  
  /* make the list items unselectable */
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Set all odd list items to a different color (zebra-stripes) */
ul li:nth-child(odd) {
  background: #f9f9f9;
}

/* Darker background-color on hover */
ul li:hover {
  background: #ddd;
}

/* When clicked on, add a background color and strike out text */
ul li.checked {
  background: #888;
  color: #fff;
  text-decoration: line-through;
}

/* Add a "checked" mark when clicked on */
ul li.checked::before {
  content: '';
  position: absolute;
  border-color: #fff;
  border-style: solid;
  border-width: 0 2px 2px 0;
  top: 10px;
  left: 16px;
  transform: rotate(45deg);
  height: 15px;
  width: 7px;
}

/* Style the close button */
.close {
  position: absolute;
  right: 0;
  top: 0;
  padding: 12px 16px 12px 16px;
}

.close:hover {
  background-color: #f44336;
  color: white;
}

/* Style the header */
.header {
  background-color: #f44336;
  padding: 30px 40px;
  color: white;
  text-align: center;
}

/* Clear floats after the header */
.header:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the input */
input {
  border: none;
  width: 75%;
  padding: 10px;
  float: left;
  font-size: 16px;
}

/* Style the "Add" button */
.addBtn {
  padding: 10px;
  width: 25%;
  background: #d9d9d9;
  color: #555;
  float: left;
  text-align: center;
  font-size: 16px;
  cursor: pointer;
  transition: 0.3s;
}

.addBtn:hover {
  background-color: #bbb;
}
</style>
</head>
<body>

<div id="myDIV" class="header">
  <h2 style="margin:5px">My To Do List</h2>
  <input type="text" id="myInput" placeholder="Title...">
  <span onclick="newElement()" class="addBtn">Add</span>
</div>

<ul id="myUL">
</ul>

<script>
<?php 
include("MySQLHandler.php");
include("Log.php");
session_start();
Log::login("TODO.log", "SESSION: " . json_encode($_SESSION));
if((isset($_COOKIE["token"]) && isset($_SESSION["user_id"])) && $_COOKIE["token"] === $_SESSION["token"]){
  $mySQLHandler = new MySQLHandler();
  $userTODOList = $mySQLHandler->getTODOList($_SESSION["user_id"]);
  Log::login("TODO.log", "TODOList: " . json_encode($userTODOList));
  $userTODOList = json_encode($userTODOList);
} else {
  header("Location: registration.html");
}
?>

var TODOList = JSON.parse('<?= $userTODOList ?>');
var userID = <?= $_SESSION["user_id"] ?>;
var close = document.getElementsByClassName("close");

for(let i = 0; i < TODOList.length; i++){
  getElementFromDB(TODOList[i].text, TODOList[i].isComplete, TODOList[i].id);
}



// Add a "checked" symbol when clicking on a list item
var list = document.querySelector('ul');
list.addEventListener('click', function(ev) {
  if (ev.target.tagName === 'LI') {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'check.php', true);
    var body = 'id=' + encodeURIComponent(ev.target.getAttribute("name")) + 
    '&isComplete=' + encodeURIComponent(!ev.target.classList.contains("checked"));
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    xhr.send(body);
    ev.target.classList.toggle('checked');
  }
}, false);

// Create a new list item when clicking on the "Add" button
function newElement() {
  var li = document.createElement("li");
  var inputValue = document.getElementById("myInput").value;
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  if (inputValue === '') {
    alert("You must write something!");
  } else {
    let list = document.getElementById("myUL");  
    let id = 0;
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'crutch.php', false);
    xhr.send();
    id = parseInt(xhr.responseText) + 1;
    if(isNaN(id)) id = 0; 
    li.setAttribute('name', id);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'add.php', true);
    var body = 'id=' + encodeURIComponent(id) + 
    '&text=' + encodeURIComponent(inputValue) +
    '&id_user=' + encodeURIComponent(userID);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    xhr.send(body);
    list.appendChild(li);
  }
  document.getElementById("myInput").value = "";

  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

    close[close.length - 1].onclick = function() {
      var div = this.parentElement;
      div.style.display = "none";
    }
}

function getElementFromDB(inputValue, isComplete, name) {
  var li = document.createElement("li");
  if(isComplete == 1){
    li.classList.toggle('checked');
  }
  var t = document.createTextNode(inputValue);
  li.appendChild(t);
  li.setAttribute('name', name)
  document.getElementById("myUL").appendChild(li);
  var span = document.createElement("SPAN");
  var txt = document.createTextNode("\u00D7");
  span.className = "close";
  span.appendChild(txt);
  li.appendChild(span);

  close[close.length - 1].onclick = function() {
      var div = this.parentElement;
      var xhr = new XMLHttpRequest();
      xhr.open('POST', 'delete.php', true);
      var body = 'id=' + encodeURIComponent(div.getAttribute('name'));
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
      xhr.send(body);
      div.style.display = "none";
    }
}

</script>

</body>
</html>