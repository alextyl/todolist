<?php
include("Log.php");

class MySQLHandler{
    const HOST = "localhost:31006";
    const USER = "root";
    const PASSWORD = "";
    const DATABASE = "tododb";

    private $link = null;

    public function __construct()
    {
        $this->link = mysqli_connect(self::HOST, self::USER, self::PASSWORD, self::DATABASE)
        or die("CONNECT ERROR: " . mysqli_error($this->link));
    }

    public function getMaxTaskID(){
        $sqlQuery = "SELECT MAX(id) as `id` FROM `tododb`.`tasks`";
        return mysqli_fetch_assoc(mysqli_query($this->link, $sqlQuery));
    }

    public function deleteTask($id){
        $id = $this->escape($id);
        $sqlQuery = "DELETE FROM `tododb`.`tasks` WHERE `id` = $id";
        Log::login("delete.log", $sqlQuery);
        return mysqli_query($this->link, $sqlQuery);
    }

    public function insertTask($id, $id_user, $text){
        $id = $this->escape($id);
        $id_user = $this->escape($id_user);
        $text = $this->escape($text);
        $sqlQuery = "INSERT INTO `tododb`.`tasks`
        (`id`,
        `id_user`,
        `text`,
        `isComplete`)
        VALUES
        ($id,
        $id_user,
        $text,
        0);
        ";
        Log::login("add.log", $sqlQuery);
        return mysqli_query($this->link, $sqlQuery);
    }

    public function changeIsComplete($id, $isComplete){
        $id = $this->escape($id);
        ($isComplete === "true") ? $isComplete = 1 : $isComplete = 0; 
        $isComplete = $this->escape($isComplete);
        $sqlQuery = "UPDATE `tododb`.`tasks` SET `isComplete` = $isComplete WHERE `id` = $id";
        Log::login("check.log", $sqlQuery);
        return mysqli_query($this->link, $sqlQuery);
    }

    public function getTODOList($user_id){
        $user_id = $this->escape($user_id);
        $sqlQuery = "SELECT * FROM `tododb`.`tasks` WHERE `id_user` = $user_id";
        Log::login("TODO.log", $sqlQuery);
        $result = mysqli_query($this->link, $sqlQuery);
        while($row = mysqli_fetch_assoc($result)){
            $TODOList[] = $row; 
        }
        return $TODOList;
    }

    public function authorization($loginOrEmail, $password){
        $loginOrEmail = $this->escape($loginOrEmail);
        $sqlQuery = "SELECT * FROM `tododb`.`users` WHERE `login` = $loginOrEmail OR `email` = $loginOrEmail";
        Log::login("authorization.log", $sqlQuery);
        $result = mysqli_query($this->link, $sqlQuery);
        if($result->num_rows === 0){
            Log::login("authorization.log", "No user founded!");
            return false;
        }
        $result = mysqli_fetch_assoc($result);
        Log::login("authorization.log", json_encode($result));
        if(hash("sha256", $password) === $result["password"]){
            Log::login("authorization.log", "Password correct!");
            return $result;
        } else {
            Log::login("authorization.log", "Password uncorrect!");
            return false;
        }
    }

    public function registration($login, $email, $password, $name = NULL, $surname = NULL, $phone = NULL){
        $name = $this->escape($name);
        $surname = $this->escape($surname);
        $phone = $this->escape($phone);
        $login = $this->escape($login);
        $email = $this->escape($email);
        $password = hash("sha256", $password);
        $password = $this->escape($password);
        $sqlQuery = "SELECT * FROM `tododb`.`users` WHERE `login` = $login OR `email` = $email";
        Log::login("registration.log", $sqlQuery);
        $result = mysqli_query($this->link, $sqlQuery);
        if($result->num_rows > 0){
            Log::login("registration.log", "Such user already has!");
            return false;
        }
        $sqlQuery = "INSERT INTO `tododb`.`users`
        (`name`,
        `surname`,
        `phone`,
        `email`,
        `login`,
        `password`)
        VALUES
        ($name,
        $surname,
        $phone,
        $email,
        $login,
        $password);";
        Log::login("registration.log", $sqlQuery);
        return mysqli_query($this->link, $sqlQuery);
    }

    public function __destruct()
    {
        mysqli_close($this->link);
    }

    private function escape($row){
        return "\""  .$row . "\"";
    }

}

?>