<?php
include("MySQLHandler.php");
include("Log.php");

Log::login("delete.log", json_encode($_POST));

$mySQLHandler = new MySQLHandler();
$mySQLHandler->deleteTask($_POST["id"]);
?>