<?php 
include("MySQLHandler.php");
include("Log.php");

Log::login("registration.log", "POST: " . json_encode($_POST));

if(!empty($_POST["name"]) && strlen($_POST["name"]) > 50) {
    Log::login("registration.log", "Registration is not success, Too long name");
    $mesages[] = "Too long name";
}

if(!empty($_POST["surname"]) && strlen($_POST["surname"]) > 50) {
    Log::login("registration.log", "Registration is not success, Too long surname");
    $mesages[] = "Too long surname";
}

if(!filter_var($_POST["email"],FILTER_VALIDATE_EMAIL)) {
    Log::login("registration.log", "Registration is not success, Not valid email");
    $mesages[] = "Not valid email";
}

if(!empty($_POST["phone"]) && !preg_match ('/^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$/' ,$_POST["phone"])) {
    Log::login("registration.log", "Registration is not success, Not valid phone");
    $mesages[] = "Not valid phone";
}

if(!preg_match ('/^[A-Za-z0-9._-]+$/' ,$_POST["login"])) {
    Log::login("registration.log", "Registration is not success, Not valid login");
    $mesages[] = "Not valid login";
}

if(!preg_match ('/^(?=.*[A-Za-z])(?=.*\d).{6,}$/' ,$_POST["password"])) {
    Log::login("registration.log", "Registration is not success, Not valid password");
    $mesages[] = "Not valid password";
}

if($_POST["check_password"] !== $_POST["password"]) {
    Log::login("registration.log", "Registration is not success, Password's must be equals.");
    $mesages[] = "Password's must be equals.";
}

if(count($mesages) > 0) {
    die(json_encode(array("isSuccess" => false, "message" => $mesages)));
}

$mySQLHandler = new MySQLHandler();
$result = $mySQLHandler->registration($_POST["login"], $_POST["email"], $_POST["password"], $_POST["name"], $_POST["surname"], $_POST["phone"]);
if($result){
    Log::login("registration.log", "Registration is success");
    $mesages[] = "Registration is success";
    die(json_encode(array("isSuccess" => true, "message" => $mesages)));
} else {
    Log::login("registration.log", "Registration is not success, SQL error");
    $mesages[] = "Registration is not success, SQL error";
    die(json_encode(array("isSuccess" => false, "message" => $mesages)));
}
?>