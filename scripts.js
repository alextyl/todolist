  $("#registration_form").submit(function(e) {
        
      e.preventDefault(); 
      console.log(e);
      var url = "registration.php"; 
      $.ajax({
            type: "POST",
            url: url,
            data: $("#registration_form").serialize(), 
            success: function(data)
            {
                data = jQuery.parseJSON(data);
                console.log("data: " + data);
                $('#modal-footer-registration').html("");
                data.message.forEach(element => {
                  if(data.isSuccess){
                    $('#modal-footer-registration').append( "<p style='color: green'>" + element + "</p>");
                  } else {
                    $('#modal-footer-registration').append( "<p style='color: red'>" + element + "</p>");
                  }
                });
            },
            done: function(response) {
              console.log(response);
            },
           errror: function (response) {
                console.log(response);
           }
          });
    });
    $("#authorization_form").submit(function(e) {
      e.preventDefault(); 
      console.log(e);
      var url = "authorization.php"; 
      $.ajax({
            type: "POST",
            url: url,
            data: $("#authorization_form").serialize(), 
            success: function(data)
            {
                data = jQuery.parseJSON(data);
                console.log("data: " + data);
                $('#modal-footer-authorization').html("");
                if(!data.isSuccess){
                $('#modal-footer-authorization').append( "<p style='color: red'>" + "INVALID LOGIN OR PASSWORD" + "</p>"); 
                } else {
                    Cookies.set('token', data.token);
                    window.location.href = "jQuery todo list.php";
                }
            },
            done: function(response) {
              console.log(response);
            },
           errror: function (response) {
                console.log(response);
           }
          });
    });
      $('#registration_form').validate({
        rules: {
            name:{
              maxlength:50
            },
            surname:{
              maxlength:50
            },
            email:{
              required: true,
              email: true
            },
            phone:{
              pattern: /(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})/
            },
            login:{
              required: true
            },
            password:{
              required: true,
              minlength:6,
              pattern: /^(?=.*[A-Za-z])(?=.*\d).{6,}$/
            },
            check_password:{
              required: true,
              equalTo: "#reg_password"
            }
        },
        messages:{
          password:{
            pattern: "Please check: min 1 number and 1 numeral."
          },
          check_password:{
            equalTo: "Password's must be equals."
          },
          phone:{
            pattern: "Correct format is +375XXXXXXXXX"
          }
        } 
      });
      $('#authorization_form').validate({
        rules:{
          login:{
            required: true
          },
          password:{
            required: true,
            minlength:6,
            pattern: /^(?=.*[A-Za-z])(?=.*\d).{6,}$/
          }
        },
        messages:{
          password:{
            pattern:"Please check: min 1 number and 1 numeral."
          }
        }
      });